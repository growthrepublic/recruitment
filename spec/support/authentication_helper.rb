module AuthenticationHelper
  def sign_in(user)
    session[:user_id] = user.id
  end

  def current_user
    @user ||= User.find_by(id: session[:user_id]) if session[:user_id]
  end

  def user_signed_in?
    !!current_user
  end
end
