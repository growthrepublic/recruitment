require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  fixtures :users

  let(:user) { users(:user) }

  before { sign_in user }


  describe 'GET #destroy' do
    it 'destroys the requested user' do
      expect {
        get :destroy, { id: user.to_param }
      }.to change(User, :count).by(-1)
    end
  end
end
