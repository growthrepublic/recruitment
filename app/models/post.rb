class Post < ActiveRecord::Base
  belongs_to :user

  scope :longer_than, lambda { |length| where("length('body') > #{length}") }

  validates :title, :body, presence: true
end
