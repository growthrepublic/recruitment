class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def sign_in
    if request.post?
      user = User.find_by(params[:user])
      if user.present?
        session[:user_id] = user.id
        redirect_to root_path, notice: 'You are now logged in'
      else
        flash.now[:notice] = 'Username or password are invalid'
      end
    end
  end

  def sign_up
    if request.post?
      @user = User.new user_params
      if @user.save
        session[:user_id] = @user.id
        redirect_to root_path, notice: 'Your account has been created'
      else
        flash.now[:notice] = @user.errors.full_messages.join(' ')
      end
    else
      @user = User.new
    end
  end

  def sign_out
    session[:user_id] = nil
    redirect_to root_path, notice: 'You have been signed out'
  end

  protected
  def user_params
    params[:user].permit!
  end
end
