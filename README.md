# Zadanie rekrutacyjne Untitled Kingdom

W kontrolerach (`UsersController`, `PostsController`), modelach (`User`, `Post`)  i w widokach (`posts`), ukryte jest kilka błędów, w tym takie, które stanowią zagrożenie dla bezpieczeństwa aplikacji. Wykryj je i popraw gdzie to jest możliwe lub pozostaw komentarz opisujący dlaczego dany kod jest błędny.

Druga część zadania to naprawienie testów w `PostsControllerSpec`, przydatna tu będzie umiejętność debugowania aplikacji przy pomocy `binding.pry`.

## Jak przesłać rozwiązanie

1. Sklonuj repozytorium lokalnie, stwórz własnego brancha jeśli chcesz
2. Dokonaj koniecznych zmian i stwórz git bundle
3. Podeślij spakowany git bundle przez e-mail

